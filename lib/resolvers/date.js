const { GraphQLScalarType } = require('graphql')
const { Kind } = require('graphql/language')

module.exports = new GraphQLScalarType({
  name: 'Date',
  parseValue (value) {
    // Value from the client
    return parseInt(value, 10)
  },
  serialize (value) {
    // Value sent to the client
    return value instanceof Date ? value.getTime() : parseInt(value, 10)
  },
  parseLiteral (ast) {
    console.log('\nsince.gql/resolvers/date.parseLiteral')
    console.log(ast);
    console.log();

    if (ast.kind === Kind.INT) {
      // AST value is always in string format
      return parseInt(ast.value, 10)
    }

    return null
  }
})
