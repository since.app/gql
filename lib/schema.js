// Package imports
const fs = require('fs')
const path = require('path').resolve
const { promisify } = require('util')

/**
 * Shortcut async version of fs.readdir.
 *
 * @type {Function}
 */
const readdir = promisify(fs.readdir)

/**
 * Shortcut async version of fs.readFile.
 *
 * @type {Function}
 */
const readfile = promisify(fs.readFile)

/**
 * RegExp to filter .graphql file types only.
 *
 * @type {RegExp}
 */
const ext = /.graphql$/

/**
 * Array map function generator for reading files in a directory.
 *
 * @function files
 * @param  {String} dir Directory to bind to
 * @returns {Promise<String>}
 */
const files = (dir) => async (file) => readfile(path(dir, file), 'utf-8')

/**
 * Array filter to return files ending with .graphql only.
 *
 * @function isgql
 * @param  {String} file File name to test
 * @returns {Boolean}
 */
const isgql = (file) => ext.test(file)

/**
 * Creates a schema builder from the list of directories.
 *
 * @function schema
 * @param  {String[]} dirs Directories to load
 * @returns {Promise<String>}
 */
module.exports = function schema (dirs) {
  /**
   * Async function to build our GraphQL schema out of .graphql files.
   *
   * @async
   * @function build
   * @returns {Promise<String>}
   */
  return async function build () {
    // Read file promises collector(concat since node doesn't have Array.flat())
    let reads = []

    // Iterate over every directory we want to load relative to this directory
    for (let i = 0; i < dirs.length; i++) {
      // Convert directory files into fs.readFile promises and appened it to our queue
      reads = reads.concat((await readdir(dirs[i])).filter(isgql).map(files(dirs[i])))
    }

    // Return the full schema as a single string
    return (await Promise.all(reads)).join('\n')
  }
}
