// Package imports
const gql = require('mercurius')

/**
 * Plugin wrapper to register mercurius(fastify-gql).
 *
 * @function graphql
 * @param   {Promise<String>} build     GraphQL schema builder
 * @param   {Object}          resolvers Resolvers for the GraphQL schema
 * @returns {Function}
 */
module.exports = function graphql (build, resolvers) {
  /**
   * Plugin to register mercurius(fastify-gql) after building our GraphQL schema.
   *
   * @async
   * @function plugin
   * @param {Context}  fastify Inherited context
   * @param {Object}   options Plugin options passed on registration
   * @param {Function} done    Completes plugin registration
   */
  return async function plugin (fastify, options, done) {
    /**
     * Timestamp since module started booting.
     *
     * @type {Number}
     */
    const t = Date.now()

    // Let console know if we're running with GraphiQL enabled
    if (options.graphiql) {
      // Sends a warning message since this should be disabled on production
      fastify.log.warn('WARNING: mercurius(fastify-gql) was registered with graphiql enabled')
    }

    try {
      // Build async schema from .graphql files
      const schema = await build()

      // Log that we're done building the GraphQL schema
      fastify.log.info('Schema built in ' + (Date.now() - t) + 'ms')

      // Register GraphQL plugin now that we have our schema
      fastify.register(gql, { graphiql: options.graphiql, resolvers, schema })

      // Let Fastify know we're done
      done()
    } catch (e) {
      // Display error in console
      fastify.log.error(e)
    }
  }
}
