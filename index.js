// Module imports
const graphql = require('./lib/graphql')
const schema = require('./lib/schema')
const resolvers = require('./lib/resolvers')

// Module exports our GraphQL plugin by default
module.exports = graphql

// Module also exports a schema builder helper function
module.exports.schema = schema

// Module also exports custom GraphQL types
module.exports.resolvers = resolvers
